package at.ac.tuwien.sepm.assignment.individual.base;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BaseTest {

	private static String currWorkingDir = null;

	@Before
	public void setSystemPropertiesForTests() {
		System.setProperty("at.ac.tuwien.sepm.testmode", "true");
		System.setProperty("at.ac.tuwien.sepm.db_log", "false");
	}

	protected static String getCurrWorkingDir() {
		if (currWorkingDir == null) {
			currWorkingDir = System.getProperty("user.dir");
		}
		return currWorkingDir;
	}

	@Test
	public void test() {
		Assert.assertTrue(true);
	}

}
