# SEPM Einzelphase

## Author

Name: Steindl, Bernhard\
Matrikelnummer: e01529136

## Kurzanleitung

Das Programm kann mit dem Befehl:\
```mvnw compile```\
gebaut werden.

Das Programm kann mit dem Befehl:\
```mvnw test```\
getestet werden.

Das Programm kann mit dem Befehl:\
```mvnw exec:java```\
ausgeführt werden.


Mit dem Befehl:\
```mvnw exec:java -Dexec.mainClass="org.h2.tools.Server" -Dexec.args="-web -browser"```\

können Sie die H2 WebUI starten sobald Sie h2 als dependency zu Ihrem Projekt hinzugefügt haben. Damit können Sie sich Ihre Datenbank ansehen.